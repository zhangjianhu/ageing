#!/usr/bin/python
# The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
#
#   This example program shows how to find frontal human faces in an image and
#   estimate their pose.  The pose takes the form of 68 landmarks.  These are
#   points on the face such as the corners of the mouth, along the eyebrows, on
#   the eyes, and so forth.
#
#   The face detector we use is made using the classic Histogram of Oriented
#   Gradients (HOG) feature combined with a linear classifier, an image pyramid,
#   and sliding window detection scheme.  The pose estimator was created by
#   using dlib's implementation of the paper:
#      One Millisecond Face Alignment with an Ensemble of Regression Trees by
#      Vahid Kazemi and Josephine Sullivan, CVPR 2014
#   and was trained on the iBUG 300-W face landmark dataset (see
#   https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/):  
#      C. Sagonas, E. Antonakos, G, Tzimiropoulos, S. Zafeiriou, M. Pantic. 
#      300 faces In-the-wild challenge: Database and results. 
#      Image and Vision Computing (IMAVIS), Special Issue on Facial Landmark Localisation "In-The-Wild". 2016.
#   You can get the trained model file from:
#   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2.
#   Note that the license for the iBUG 300-W dataset excludes commercial use.
#   So you should contact Imperial College London to find out if it's OK for
#   you to use this model file in a commercial product.
#
#
#   Also, note that you can train your own models using dlib's machine learning
#   tools. See train_shape_predictor.py to see an example.
#
#
# COMPILING/INSTALLING THE DLIB PYTHON INTERFACE
#   You can install dlib using the command:
#       pip install dlib
#
#   Alternatively, if you want to compile dlib yourself then go into the dlib
#   root folder and run:
#       python setup.py install
#   or
#       python setup.py install --yes USE_AVX_INSTRUCTIONS
#   if you have a CPU that supports AVX instructions, since this makes some
#   things run faster.  
#
#   Compiling dlib should work on any operating system so long as you have
#   CMake and boost-python installed.  On Ubuntu, this can be done easily by
#   running the command:
#       sudo apt-get install libboost-python-dev cmake
#
#   Also note that this example requires scikit-image which can be installed
#   via the command:
#       pip install scikit-image
#   Or downloaded from http://scikit-image.org/download.html. 

import sys
import os
import dlib
import glob
from skimage import io

if len(sys.argv) != 3:
    print(
        "Give the path to the trained shape predictor model as the first "
        "argument and then the directory containing the facial images.\n"
        "For example, if you are in the python_examples folder then "
        "execute this program by running:\n"
        "    ./face_landmark_detection.py shape_predictor_68_face_landmarks.dat ../examples/faces\n"
        "You can download a trained facial shape predictor from:\n"
        "    http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2")
    exit()

predictor_path = sys.argv[1]
faces_folder_path = sys.argv[2]

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(predictor_path)
# win = dlib.image_window()

for f in glob.glob(os.path.join(faces_folder_path, "*.png")):
    print("Processing file: {}".format(f))
    try:
        img = io.imread(f)
    except Exception as e:
        os.remove(f)
        continue
    
    height, width, channels = img.shape
    # win.clear_overlay()
    # win.set_image(img)

    # Ask the detector to find the bounding boxes of each face. The 1 in the
    # second argument indicates that we should upsample the image 1 time. This
    # will make everything bigger and allow us to detect more faces.
    dets = detector(img, 0)
    if len(dets) != 1:
        os.remove(f)
        continue
    # print("Number of faces detected: {}".format(len(dets)))
    for k, d in enumerate(dets):
        # print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
        #     k, d.left(), d.top(), d.right(), d.bottom()))
        # Get the landmarks/parts for the face in box d.
        shape = predictor(img, d)
        with open(f+".txt",'w') as txt:
            for index in range(0, 68):
                txt.write("{} {}\n".format(shape.part(index).x, shape.part(index).y))

            ting = ( 2*shape.part(8).y - shape.part(19).y - shape.part(24).y ) / 4
            yan = ( shape.part(39).x - shape.part(36).x + shape.part(45).x - shape.part(42).x) / 2

            x68 = (shape.part(19).x + shape.part(24).x)/2
            y68 = (shape.part(19).y + shape.part(24).y)/2 - ting*2/3

            x69 = shape.part(36).x - yan/2
            y69 = shape.part(19).y - ting/2

            x70 = shape.part(45).x + yan/2
            y70 = y69

            x71 = (x69 + shape.part(0).x)/2 - yan/2
            y71 = (y69 + shape.part(0).y)/2

            x72 = (x70 + shape.part(16).x)/2 + yan/2
            y72 = (y70 + shape.part(16).y)/2

            x73 = (x68+x69)/2 - yan/4
            y73 = (y68+y69)/2 - yan/4

            x74 = (x68+x70)/2 + yan/4
            y74 = (y68+y70)/2 - yan/4

            txt.write("{} {}\n".format(x68,y68))
            txt.write("{} {}\n".format(x69,y69))
            txt.write("{} {}\n".format(x70,y70))
            txt.write("{} {}\n".format(x71,y71))
            txt.write("{} {}\n".format(x72,y72))
            txt.write("{} {}\n".format(x73,y73))
            txt.write("{} {}\n".format(x74,y74))

            txt.write("{} {}\n".format(0,0))
            txt.write("{} {}\n".format(width/2,0))
            txt.write("{} {}\n".format(width-1,0))
            txt.write("{} {}\n".format(0,height/2))
            txt.write("{} {}\n".format(width-1,height/2))
            txt.write("{} {}\n".format(0,height-1))
            txt.write("{} {}\n".format(width/2,height-1))
            txt.write("{} {}\n".format(width-1,height-1))

            # txt.write("{} {}\n".format(d.left(), d.top()))
            # txt.write("{} {}\n".format(d.right(), d.bottom()))
        # print("Part 0: {}, Part 1: {} ...".format(shape.part(0),
        #                                           shape.part(1)))
        # Draw the face landmarks on the screen.
        # win.add_overlay(shape)

    # win.add_overlay(dets)
    # dlib.hit_enter_to_continue()
