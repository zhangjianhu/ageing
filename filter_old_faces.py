import glob
import os
import sys
from shutil import copyfile


# Read in the points from a text file
count = 0
for path in glob.glob(os.path.join("../Downloads/imdb_crop/*/", "*.jpg")):
	filename = path.split('/')[-1]
	filename_piece = filename.split('_')
	birth_year = int(filename_piece[-2].split('-')[0])
	photo_taken = int(filename_piece[-1].split('.')[0])
	age = photo_taken - birth_year
	if age >= 60:
		count += 1
		print(count)
		copyfile(path, os.path.join("../Downloads/old_faces",filename))