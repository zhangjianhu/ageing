#!/usr/bin/python

import cv2
import numpy as np
import random
import glob
import os
import sys
mouth = [49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68]
left_eye = [37,38,39,40,41,42]
right_eye = [43,44,45,46,47,48]
triangles = []
# Check if a point is inside a rectangle
def rect_contains(rect, point) :
    if point[0] < rect[0] :
        return False
    elif point[1] < rect[1] :
        return False
    elif point[0] > rect[2] :
        return False
    elif point[1] > rect[3] :
        return False
    return True

# Draw a point
def draw_point(img, p, color, text ) :
    # cv2.circle( img, p, 2, color, cv2.FILLED, cv2.LINE_AA, 0 )
    cv2.putText( img, str(text), p, cv2.FONT_HERSHEY_SIMPLEX, 0.25, color )

# Draw delaunay triangles
def draw_delaunay(img, subdiv, delaunay_color ) :

    triangleList = subdiv.getTriangleList();
    size = img.shape
    r = (0, 0, size[1], size[0])

    for t in triangleList :
        
        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])
        
        if rect_contains(r, pt1) and rect_contains(r, pt2) and rect_contains(r, pt3) :
        
            cv2.line(img, pt1, pt2, delaunay_color, 1, cv2.LINE_AA, 0)
            cv2.line(img, pt2, pt3, delaunay_color, 1, cv2.LINE_AA, 0)
            cv2.line(img, pt3, pt1, delaunay_color, 1, cv2.LINE_AA, 0)

            triangles.append([pt1,pt2,pt3])

# Draw voronoi diagram
def draw_voronoi(img, subdiv) :

    ( facets, centers) = subdiv.getVoronoiFacetList([])

    for i in xrange(0,len(facets)) :
        ifacet_arr = []
        for f in facets[i] :
            ifacet_arr.append(f)
        
        ifacet = np.array(ifacet_arr, np.int)
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

        cv2.fillConvexPoly(img, ifacet, color, cv2.LINE_AA, 0);
        ifacets = np.array([ifacet])
        cv2.polylines(img, ifacets, True, (0, 0, 0), 1, cv2.LINE_AA, 0)
        cv2.circle(img, (centers[i][0], centers[i][1]), 3, (0, 0, 0), cv2.FILLED, cv2.LINE_AA, 0)


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print(
            "Give the path to the directory containing the facial images and the landmark texts as the first "
            "argument \n")
        exit()

    
    faces_folder_path = sys.argv[1]
    # Turn on animation while drawing triangles
    animate = False
    
    # Define colors for drawing.
    delaunay_color = (255,255,255)
    points_color = (0, 0, 255)


    
    # Read in the points from a text file
    for image_path in glob.glob(os.path.join(faces_folder_path, "*.png")):
        print(image_path)
        triangles = []
        # Define window names
        win_delaunay = image_path
        win_voronoi = "Voronoi Diagram"
        # Read in the image.
        img = cv2.imread(image_path);
        
        # Keep a copy around
        img_orig = img.copy();
        
        # Rectangle to be used with Subdiv2D
        size = img.shape
        rect = (0, 0, size[1], size[0])
        
        # Create an instance of Subdiv2D
        subdiv = cv2.Subdiv2D(rect);

        # Create an array of points.
        points = [];
        with open(image_path +".txt") as file :
            for line in file :
                x, y = line.split()
                points.append((int(x), int(y)))

        # Insert points into subdiv
        try:
            for p in points :
                subdiv.insert(p)
        except Exception as e:
            os.remove(image_path)
            continue

            
            # Show animation
            if animate :
                img_copy = img_orig.copy()
                # Draw delaunay triangles
                draw_delaunay( img_copy, subdiv, (255, 255, 255) );
                cv2.imshow(win_delaunay, img_copy)
                cv2.waitKey(100)

        # Draw delaunay triangles
        draw_delaunay( img, subdiv, (255, 255, 255) );

        # Draw points
        p_index = 0
        for p in points :
            p_index += 1
            draw_point(img, p, (0,0,255), p_index)

        # left_top = points[-2]
        # right_bottom = points[-1]

        # w = right_bottom[0] - left_top[0]
        # h = right_bottom[1] - left_top[1]
        
        # right_most = size[1] - right_bottom[0]
        # bottom_most = size[0] - right_bottom[1]
        # min1 = np.min([right_most, bottom_most])
        # left_most = left_top[0]
        # top_most = left_top[1]
        # min2 = np.min([left_most, top_most])

        # new_left_top = ( left_top[0] - min2 , left_top[1] - min2)
        # new_right_bottom = ( right_bottom[0] + min1, right_bottom[1] + min1)

        # # print(new_left_top)
        # # print(new_right_bottom)
        # # print(size)

        # crop_img = img[new_left_top[1]:new_right_bottom[1],new_left_top[0]:new_right_bottom[0]]
        

        # # cv2.rectangle(img, new_left_top, new_right_bottom, (0,0,255))

        # resized_img = cv2.resize(crop_img, (1000,1000))
        # cv2.imwrite(image_path+'.cropped.png',crop_img)
        # cv2.imwrite(image_path+'.resizeed.png',resized_img)

        with open(image_path + ".tri.txt",'w') as file:
            tmp = []
            for t in triangles:
                p_index = 0
                for p in points:
                    p_index += 1
                    if t[0] == p:
                        t[0] = p_index
                    elif t[1] == p:
                        t[1] = p_index
                    elif t[2] == p:
                        t[2] = p_index
                list.sort(t)
                tmp.append(t)
            list.sort(tmp)
            for t in tmp:
                # if t[0] in mouth and t[1] in mouth and t[2] in mouth:
                #     continue
                # if t[0] in left_eye and t[1] in left_eye and t[2] in left_eye:
                #     continue
                # if t[0] in right_eye and t[1] in right_eye and t[2] in right_eye:
                #     continue
                file.write(" ".join(str(x) for x in t)+"\n")
        # Allocate space for voronoi Diagram
        img_voronoi = np.zeros(img.shape, dtype = img.dtype)

        # Draw voronoi diagram
        # draw_voronoi(img_voronoi,subdiv)

        # Show results
        # cv2.imshow(win_delaunay,img)
        # cv2.imshow(win_voronoi,img_voronoi)
        # cv2.waitKey(0)
        cv2.imwrite(image_path+'.png',img)


