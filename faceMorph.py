#!/usr/bin/env python

import numpy as np
import cv2
import sys
import dlib
import glob
import os
import random
from skimage import io

mouth = [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68]
left_eye = [37, 38, 39, 40, 41, 42]
right_eye = [43, 44, 45, 46, 47, 48]


# Check if a point is inside a rectangle
def rect_contains(rect, point):
    if point[0] < rect[0]:
        return False
    elif point[1] < rect[1]:
        return False
    elif point[0] > rect[2]:
        return False
    elif point[1] > rect[3]:
        return False
    return True


# Read points from text file

def read_points(path):
    # Create an array of points.
    points = []
    # Read points
    with open(path) as f:
        for line in f:
            x, y = line.split()
            points.append((int(x), int(y)))

    return points


# Apply affine transform calculated using srcTri and dstTri to src and
# output an image of size.
def apply_affine_transform(src, src_tri, dst_tri, size):
    # Given a pair of triangles, find the affine transform.
    warp_mat = cv2.getAffineTransform(np.float32(src_tri), np.float32(dst_tri))

    # Apply the Affine Transform just found to the src image
    dst = cv2.warpAffine(src, warp_mat, (size[0], size[1]), None, flags=cv2.INTER_LINEAR,
                         borderMode=cv2.BORDER_REFLECT_101)

    return dst


# Warps and alpha blends triangular regions from img1 and img2 to img
def morph_triangle(img1, img2, img3, img, t1, t2, t3, t, alpha, belta):
    # Find bounding rectangle for each triangle
    r1 = cv2.boundingRect(np.float32([t1]))
    r2 = cv2.boundingRect(np.float32([t2]))
    r3 = cv2.boundingRect(np.float32([t3]))
    r = cv2.boundingRect(np.float32([t]))

    # Offset points by left top corner of the respective rectangles
    t1_rect = []
    t2_rect = []
    t3_rect = []
    t_rect = []

    for i in xrange(0, 3):
        t_rect.append(((t[i][0] - r[0]), (t[i][1] - r[1])))
        t1_rect.append(((t1[i][0] - r1[0]), (t1[i][1] - r1[1])))
        t2_rect.append(((t2[i][0] - r2[0]), (t2[i][1] - r2[1])))
        t3_rect.append(((t3[i][0] - r3[0]), (t3[i][1] - r3[1])))

    # Get mask by filling triangle
    mask = np.zeros((r[3], r[2], 3), dtype=np.float32)
    cv2.fillConvexPoly(mask, np.int32(t_rect), (1.0, 1.0, 1.0), 16, 0);

    # Apply warpImage to small rectangular patches
    img1_rect = img1[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
    img2_rect = img2[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]]
    img3_rect = img3[r3[1]:r3[1] + r3[3], r3[0]:r3[0] + r3[2]]

    size = (r[2], r[3])
    warp_image1 = apply_affine_transform(img1_rect, t1_rect, t_rect, size)
    warp_image2 = apply_affine_transform(img2_rect, t2_rect, t_rect, size)
    warp_image3 = apply_affine_transform(img3_rect, t3_rect, t_rect, size)

    # Alpha blend rectangular patches
    img_rect = (1.0 - alpha) * warp_image1 + alpha * ((1-belta)*warp_image2+belta*warp_image3)

    # Copy triangular region of the rectangular patch to the output image
    img[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] = img[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] * (1 - mask) + img_rect * mask


def detect_face_landmark(img, model_path):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(model_path)
    height, width, channels = img.shape
    dets = detector(img, 0)
    if len(dets) == 0:
        print('None face detected!')
    elif len(dets) > 1:
        print("Multiple face detected!")
    else:
        landmarks = []
        for k, d in enumerate(dets):
            shape = predictor(img, d)
            for index in range(0, 68):
                landmarks.append((shape.part(index).x, shape.part(index).y))

            ting = (2 * shape.part(8).y - shape.part(19).y - shape.part(24).y) / float(4)
            yan = (shape.part(39).x - shape.part(36).x + shape.part(45).x - shape.part(42).x) / float(2)

            x68 = (shape.part(19).x + shape.part(24).x) / float(2)
            y68 = (shape.part(19).y + shape.part(24).y) / float(2) - ting*2/3

            x69 = shape.part(36).x - yan / float(2)
            y69 = shape.part(19).y - ting / float(2)

            x70 = shape.part(45).x + yan / float(2)
            y70 = y69

            x71 = (x69 + shape.part(0).x) / float(2) - yan / float(2)
            y71 = (y69 + shape.part(0).y) / float(2)

            x72 = (x70 + shape.part(16).x) / float(2) + yan / float(2)
            y72 = (y70 + shape.part(16).y) / float(2)

            x73 = (x68 + x69) / float(2) - yan / float(4)
            y73 = (y68 + y69) / float(2) - yan / float(4)

            x74 = (x68 + x70) / float(2) + yan / float(4)
            y74 = (y68 + y70) / float(2) - yan / float(4)

            landmarks.append((int(x68), int(y68)))
            landmarks.append((int(x69), int(y69)))
            landmarks.append((int(x70), int(y70)))
            landmarks.append((int(x71), int(y71)))
            landmarks.append((int(x72), int(y72)))
            landmarks.append((int(x73), int(y73)))
            landmarks.append((int(x74), int(y74)))
            landmarks.append((0, 0))
            landmarks.append((width / 2, 0))
            landmarks.append((width - 1, 0))
            landmarks.append((0, height / 2))
            landmarks.append((width - 1, height / 2))
            landmarks.append((0, height - 1))
            landmarks.append((width / 2, height - 1))
            landmarks.append((width - 1, height - 1))
        # print(landmarks)
        return landmarks


def delaunay(points, img):
    triangles = []
    # Keep a copy around
    img_orig = img.copy();

    # Rectangle to be used with Subdiv2D
    size = img.shape
    rect = (0, 0, size[1], size[0])

    # Create an instance of Subdiv2D
    subdiv = cv2.Subdiv2D(rect)

    # Insert points into subdiv
    for p in points:
        subdiv.insert(p)

    triangle_list = subdiv.getTriangleList()

    for t in triangle_list:

        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])

        if rect_contains(rect, pt1) and rect_contains(rect, pt2) and rect_contains(rect, pt3):
            triangles.append([pt1, pt2, pt3])

    tmp = []
    for t in triangles:
        p_index = 0
        for p in points:
            p_index += 1
            if t[0] == p:
                t[0] = p_index
            elif t[1] == p:
                t[1] = p_index
            elif t[2] == p:
                t[2] = p_index
        list.sort(t)
        tmp.append(t)
    list.sort(tmp)
    return tmp


def get_similarity(t1, t2):
    res = 0
    for x in t1:
        if x in t2:
            res += 1
    return res


def get_similar_old_faces(old_faces_image, triangles, points):
    face_shape_ratio = float(points[8][1]-points[27][1]) / (points[0][0]-points[16][0])
    candidates = []
    result = []
    for old_face in glob.glob(os.path.join(old_faces_image, "*.png.txt")):
        old_face_points = []
        with open(old_face) as f:
            for line in f :
                x, y = line.split()
                old_face_points.append((int(x), int(y)))
        old_face_shape_ratio = float(old_face_points[8][1]-old_face_points[27][1]) / (old_face_points[0][0]-old_face_points[16][0])
        ratio_delta = abs(face_shape_ratio - old_face_shape_ratio)
        if len(candidates) < 10:
            candidates.append((ratio_delta, old_face[:-4]))
            candidates.sort()
        elif ratio_delta < candidates[-1][0]:
            candidates[-1] = (ratio_delta, old_face[:-4])
            candidates.sort()
    for item in candidates:
        old_face = item[1]+".tri.txt"
        t = []
        with open(old_face) as f:
            for line in f:
                line = line.strip().split(' ')
                line = [int(x) for x in line]
                t.append(line)
        result.append((old_face, get_similarity(t, triangles)))
    result.sort(key=lambda x: x[1])
    result.reverse()
    return result


def crop_image(image_path, model_path):
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(model_path)
    img = io.imread(image_path)
    origin_height, origin_width, channels = img.shape
    # print(img.shape)
    dets = detector(img, 0)
    # print(len(dets))
    if len(dets) != 1:
        return
    for k, d in enumerate(dets):
        left_top = (d.left(), d.top())
        right_bottom = (d.right(), d.bottom())

        right_most = origin_width - right_bottom[0]
        bottom_most = origin_height - right_bottom[1]
        min1 = np.min([right_most, bottom_most])
        new_right_bottom = (right_bottom[0] + min1, right_bottom[1] + min1)

        left_most = left_top[0]
        top_most = left_top[1]
        min2 = np.min([left_most, top_most])
        new_left_top = (left_top[0] - min2, left_top[1] - min2)

        return new_left_top, new_right_bottom


def get_center_point(points):
    count = 0
    x = 0
    y = 0
    for p in points:
        x += p[0]
        y += p[1]
        count += 1
    return int(float(x) / count), int(float(y) / count)


def faceMorph(target_image_path, faces_folder_path, model_path):
    alpha = 0.8
    beta = 0.5

    target_image = cv2.imread(target_image_path)
    

    crop_points = crop_image(target_image_path, model_path)
    if not crop_points:
        print("none face detected or multiple faces detected")
        return
    left_top, right_bottom = crop_points

    cropped_img = target_image[left_top[1]:right_bottom[1], left_top[0]:right_bottom[0]]
    resized_img = cv2.resize(cropped_img, (1000, 1000))

    cv2.imshow("Original Face1", np.uint8(resized_img))

    target_points = detect_face_landmark(resized_img, model_path)
    # print(target_points)
    target_triangles = delaunay(target_points, resized_img)

    old_faces = get_similar_old_faces(faces_folder_path, target_triangles, target_points)

    old_face_tri_path1 = old_faces[0][0]
    print(old_face_tri_path1)
    old_face_image_path1 = old_face_tri_path1[0:-8]
    old_face_points_path1 = old_face_image_path1 + ".txt"
    old_face1 = cv2.imread(old_face_image_path1)

    old_face_tri_path2 = old_faces[1][0]
    print(old_face_tri_path2)
    old_face_image_path2 = old_face_tri_path2[0:-8]
    old_face_points_path2 = old_face_image_path2 + ".txt"
    old_face2 = cv2.imread(old_face_image_path2)

    # Convert Mat to float data type
    old_face1 = np.float32(old_face1)
    old_face2 = np.float32(old_face2)
    resized_img = np.float32(resized_img)

    old_face_points1 = read_points(old_face_points_path1)
    old_face_points2 = read_points(old_face_points_path2)

    points = []
    # Compute weighted average point coordinates
    for i in xrange(0, len(target_points)):
        actual_alpha = alpha

        # if i >= 68 or i <= 16:
        #     actual_alpha = 0

        x = (1 - actual_alpha) * target_points[i][0] \
            + actual_alpha * ((1-beta)*old_face_points1[i][0] + beta*old_face_points2[i][0])
        y = (1 - actual_alpha) * target_points[i][1] \
            + actual_alpha * ((1-beta)*old_face_points1[i][1] + beta*old_face_points2[i][1])
        # if i==68:
        #     print(target_points[i])
        #     print(old_face_points1[i])
        #     print(old_face_points2[i])
        #     print((int(x), int(y)))
        points.append((int(x), int(y)))

    # print(points)
    mask_points = points[0:16] + [points[72], points[70], points[74], points[68], points[73], points[69], points[71]]
    mask_points = [[float(x[0]) / 1000, float(x[1]) / 1000] for x in mask_points]
    mask_points = [[int(x[0] * cropped_img.shape[0]), int(x[1] * cropped_img.shape[0])] for x in mask_points]

    rect_points = [[points[71][0], points[68][1]], [points[72][0], points[68][1]], [points[72][0], points[8][1]],
                   [points[71][0], points[8][1]]]
    rect_points = [[float(x[0]) / 1000, float(x[1]) / 1000] for x in rect_points]
    rect_points = [[int(x[0] * cropped_img.shape[0]), int(x[1] * cropped_img.shape[0])] for x in rect_points]

    center = get_center_point(rect_points)

    # Allocate space for final output
    morph_img = np.zeros(resized_img.shape, dtype=resized_img.dtype)
    fake_morph_img = np.zeros(resized_img.shape, dtype=resized_img.dtype)
    for triangle in target_triangles:
        x, y, z = triangle

        actual_alpha = alpha

        if x > 75 or y > 75 or z > 75:
            actual_alpha = 0
        # elif x in mouth and y in mouth and z in mouth:
        #     actual_alpha = 0
        # elif x in left_eye and y in left_eye and z in left_eye:
        #     actual_alpha = 0
        # elif x in right_eye and y in right_eye and z in right_eye:
        #     actual_alpha = 0

        x -= 1
        y -= 1
        z -= 1

        t1 = [target_points[x], target_points[y], target_points[z]]
        t2 = [old_face_points1[x], old_face_points1[y], old_face_points1[z]]
        t3 = [old_face_points2[x], old_face_points2[y], old_face_points2[z]]
        t = [points[x], points[y], points[z]]

        # Morph one triangle at a time.
        morph_triangle(resized_img, old_face1, old_face2, morph_img, t1, t2, t3, t, actual_alpha, beta)
        morph_triangle(resized_img, old_face1, old_face2, fake_morph_img, t1, t3, t2, t, 0, beta)

    
    morph_img = cv2.resize(morph_img, (cropped_img.shape[0], cropped_img.shape[1]))
    fake_morph_img = cv2.resize(fake_morph_img, (cropped_img.shape[0], cropped_img.shape[1]))
    old_face2 = cv2.resize(old_face2, (cropped_img.shape[0], cropped_img.shape[1]))
    old_face1 = cv2.resize(old_face1, (cropped_img.shape[0], cropped_img.shape[1]))
    # cv2.imwrite( images_folder_path + "/" + str(count)+'-origin.jpg',target_image)
    # cv2.imwrite( images_folder_path + "/" + str(count)+'-old.png',old_face1)
    morph_path = target_image_path + '-result.png'
    fake_morph_path = target_image_path + '-fake.png'
    cv2.imwrite(morph_path, morph_img)
    cv2.imwrite(fake_morph_path, fake_morph_img)
    # center = (morph_img.shape[0]/2, morph_img.shape[1]/2)

    output = poisson_edit(morph_path, fake_morph_path, mask_points, center)
    cv2.imshow("Output Face1", np.uint8(output))
    cv2.imwrite(morph_path, output)
    output = poisson_edit(morph_path, fake_morph_path, mask_points, center)
    cv2.imshow("Output Face2", np.uint8(output))
    draw_point(output, 'C', center)

    cv2.imshow("Target Face", np.uint8(fake_morph_img))
    
    cv2.imshow("Morphed Face", np.uint8(morph_img))
    
    cv2.imshow("Old Face1", np.uint8(old_face1))
    cv2.imshow("Old Face2", np.uint8(old_face2))
    draw_points(resized_img, points, (255,0,0))
    draw_points(resized_img, target_points,(0,0,255))
    cv2.imshow("Resized Face1", np.uint8(resized_img))
    cv2.waitKey(0)
    # return result_path     


def poisson_edit(src, dst, landmarks, center):
    src = cv2.imread(src)
    dst = cv2.imread(dst)

    src_mask = np.zeros(src.shape, src.dtype)
    poly = np.array(landmarks, np.int32)
    cv2.fillPoly(src_mask, [poly], (255, 255, 255))

    # src_mask = 255*np.ones(src.shape, src.dtype)

    output = cv2.seamlessClone(src, dst, src_mask, center, cv2.MIXED_CLONE)

    # print(output)
    return output


def draw_points(img, points, color = (0, 0, 255)):
    count = 0
    for p in points:
        count += 1
        draw_point(img, count, (p[0], p[1]), color)


def draw_point(img, text, p, color = (0, 0, 255)):
    p = (p[0]+5,p[1]+5)
    cv2.putText( img, str(text), p, cv2.FONT_HERSHEY_SIMPLEX, 0.25,  color)
    # cv2.circle(img, p, 2, (0, 0, 255), cv2.FILLED, cv2.LINE_AA, 0)


if __name__ == '__main__':
    target_image_path = sys.argv[1]
    # faces_folder_path = sys.argv[2]
    # model_path = sys.argv[3]
    faces_folder_path = "./old_faces"
    model_path = "./shape_predictor_68_face_landmarks.dat"
    result_path = faceMorph(target_image_path, faces_folder_path, model_path)
    print(result_path)
    # alpha = float(sys.argv[4])
