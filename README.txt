1. 环境
python2.7,安装 dlib 和 opencv：
pip install dlib
pip install opencv-python

2. 函数调用
result_path = faceMorph(target_image_path, faces_folder_path, model_path)
三个参数分别是：
	1. 要老化的目标图片路径
	2. 老人脸图片所在文件夹路径（即 old_faces）
	3. 人脸关键点检测模型文件路径（即 shape_predictor_68_face_landmarks.dat）
返回值是处理后的图片路径，PNG格式